var personArr = [
    {
        "personId" : 123,
        "name" : "Jhon",
        "city" : "Melbourne",
        "phoneNo" : "1234567890"
    },
    {
        "personId" : 124,
        "name" : "Amelia",
        "city" : "Sydney",
        "phoneNo" : "1234567890"
    },
    {
        "personId" : 125,
        "name" : "Emily",
        "city" : "Perth",
        "phoneNo" : "1234567890"
    },
    {
        "personId" : 126,
        "name" : "Abraham",
        "city" : "Perth",
        "phoneNo" : "1234567890"
    },
];


function tabla(){
  var body = document.getElementsByTagName("body")[0];
  var tabla   = document.createElement("table");
  var tbBody = document.createElement("tbody");
    
  for (var i = 0; i < 1; i++) {
    var fila = document.createElement("tr");
 
      var celda = document.createElement("td");
      var textoCelda = document.createTextNode("ID");
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode("NOMBRE");
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode("CIUDAD");
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode("TELÉFONO");
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

    tbBody.appendChild(fila);
  }
 
 
  for (var i = 0; i < personArr.length; i++) {
    var fila = document.createElement("tr");
 
      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(personArr[i].personId);
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(personArr[i].name);
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(personArr[i].city);
      celda.appendChild(textoCelda);
      fila.appendChild(celda);

      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(personArr[i].phoneNo);
      celda.appendChild(textoCelda);
      fila.appendChild(celda);
    
    tbBody.appendChild(fila);
  }
  tabla.appendChild(tbBody);
  body.appendChild(tabla);
  tabla.setAttribute("border", "1");
  tabla.width = 350;
}
tabla();

function infor(ele){
    switch( ele.id ){
        case 'C1':
                var li1 = document.getElementById('C1');
                 alert('ID elemento: {'+ ele.id + '} \n ISO ID: {' + li1.dataset.id + '} \n DIAL Code: {' + li1.dataset.dialCode + '}'); 
                 break;
        case "C2":
                var li1 = document.getElementById('C2');
                 alert('ID elemento: {'+ ele.id + '} \n ISO ID: {' + li1.dataset.id + '} \n DIAL Code: {' + li1.dataset.dialCode + '}'); 
                 break;
        case "C3":
                var li1 = document.getElementById('C3');
                 alert('ID elemento: {'+ ele.id + '} \n ISO ID: {' + li1.dataset.id + '} \n DIAL Code: {' + li1.dataset.dialCode + '}'); 
                 break;
     }
    
}